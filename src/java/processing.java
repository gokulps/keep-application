/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author dell
 */
@WebServlet(urlPatterns = {"/processing"})
public class processing extends HttpServlet {

    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException,Exception, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        String USER = "system";
        String PASS = "admin";
        String DB_URL="jdbc:oracle:thin:@localhost:1521:XE";
             
             System.out.println("hello");
             //DRIVER MANAGER - TO CONVERT JAVA APPLICATION CALLS TO DB CALLS
             Class.forName("oracle.jdbc.driver.OracleDriver");
             try{
                 //Creation of connection to send request to DB and to fetch results. Acts as a connection path
            Connection conn = DriverManager.getConnection(DB_URL,USER,PASS);
            response.setContentType("text/html");
            PrintWriter out = response.getWriter();
            int n2= Integer.parseInt(request.getParameter("n1"));
            String sql = "delete from datatable where ID = '"+n2+"'";
            Statement stmt  = conn.createStatement();
            stmt.executeUpdate(sql);
                    
                    
                
             //stmt5.close();
             conn.close();
             }catch(SQLException se){
                 se.printStackTrace();
             }catch(Exception e) {
              //Handle errors for Class.forName
              e.printStackTrace();
             } 
        
        
        
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(processing.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(processing.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
